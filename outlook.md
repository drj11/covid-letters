# Deaths

Deaths are a more realiable indicator of infection progress than
confirmed cases. Because of testing regimes.

The best data in timeseries seems to be Wikipedia:
https://en.wikipedia.org/wiki/2020_coronavirus_pandemic_in_the_United_Kingdom

# ICU Capacity

Estimate as of 2020-03-24

Last 3 days: 158 deaths

between 600,000 and 2,400,000 million cases at present.

(in fact, due to publication of figures, likely 0900 previous day).

Doubling has slowed. It took slightly more than 3 days for
deaths to double to their current number.
The change in doubling rate, makes the time estimates much less
certain.

There are approximately 5,000 ICU beds in the UK.

Approximately 10% of cases require ICU.

Between approximately 11 and 17 days there were 50,0000 cases
(note that this is earlier than our earlier estimates).
Assume a disease length of 12 days.
These cases should be filling national ICU capacity between now
and Monday next week.
Expect reports soon.

Say Wednesday 2020-03-25.
Less than two days later as many
patients again will require ICU and be unable to access it.

Because of current doubling even quite badly wrong estimate only
change the timing by days. If it's only 1% of infections that
require ICU then that buys us 5 days time.
Putting out-of-capacity date at Monday 2020-03-30

# Lockdown

On 2020-03-24 the UK went into lockdown.
Assume it takes two weeks to show some sort of inflection
change in the death curves.
In 14 days times, there will be 2**4 more deaths, bring the
total to about 2,700.
Assumes the same number of deaths on the other side of the
curve.

Totals deaths: about 5,400 (assuming it stops at all)
